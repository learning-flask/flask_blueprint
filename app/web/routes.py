from app.web import web
from app.models import Post
from flask import render_template
from datetime import datetime
from app import db

@web.route('/', methods=['GET', 'POST'])
@web.route('/home', methods=['GET', 'POST'])
def home():
    posts= Post.query.order_by("create_date desc").limit(20).all()
    return render_template("home.html", posts=posts)