from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from app.config import Config

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)  # db object that represents the database
migrate = Migrate(app, db)  # migrate is an istance of db Migrate

from app import models
from app.web import routes
from app.web import web
from app.api import routes, api

app.register_blueprint(web)
app.register_blueprint(api)
