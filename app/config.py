import os

basedir = os.path.abspath(os.path.dirname(__file__))


# it is used to config app.
# In this project, the concern should be separate

class Config(object):
    QUESTION_PER_PAGE = 2

    SECRET_KEY = os.environ.get(
        'SECRET_KEY') or 'you-will-never-guess'  # be used to be cryptographic key, generate signatures or tokens

    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
                              'sqlite:///' + os.path.join(basedir, 'app.db')  # locate of database
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # not signal the application
    ELASTICSEARCH_URL='http://localhost: 9200'