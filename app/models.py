from app import db

class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64), index=True)
    content = db.Column(db.String(120), index=True)
    create_date = db.Column(db.DateTime)

    def __repr__(self):
        return '<Post {}>'.format(self.id)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def return_one(self, id):
        def to_json(x):
            return {
                'id': x.id,
                'title': x.title,
                'content': x.content,
                'create_date': str(x.create_date)
            }
        return {'posts': list(map(lambda x: to_json(x), Post.query.filter_by(id=id)))}

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'id': x.id,
                'title': x.title,
                'content': x.content,
                'create_date': str(x.create_date)
            }
        return {'posts': list(map(lambda x: to_json(x), Post.query.all()))}

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

    @classmethod
    def delete_one(cls, id):
        post = Post.find_id(id)
        num_rows_deleted = db.session.query(cls).delete(post)
        db.session.commit()
        return {'message': '{} deleted'.format(num_rows_deleted)}


    @classmethod
    def find_id(cls, id):
        return cls.query.filter_by(id=id).first()