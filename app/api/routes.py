from flask_restful import Api, Resource

from app.api import api
from app.models import Post

API = Api(api)


class PostsResouce(Resource):

    def get(self, id=None):
        if id is None:
            return Post.return_all()
        return Post.return_one(id)

    def post(self):
        pass

    def put(self, id):
        pass

    def delete(self, id=None):
        if not id:
            return Post.delete_all()
        return Post.delete_one(id)


API.add_resource(PostsResouce, '/api/posts/', '/api/posts/<id>')
